package com.example.robert.hackathon.communication;

import com.example.robert.hackathon.model.Notification;

import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

/**
 * Created by APS Drones on 09.06.2017.
 */

public class RestMessageRetriever extends RestClientBase {

    private RestMessageRetriever() {
        super("POST", "http://172.16.102.35:8080/WrapGuardianServer/message/getMessages");
    }

    protected void prepare() {
        try {
//            JSONObject obj = msg.toJSON();
//            setData(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static JSONObject getMessages() {
        JSONObject response = null;
        RestMessageRetriever task = null;
        try {
            task = new RestMessageRetriever();
            task.prepare();
            task.execute();
            response = task.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return response;
    }
}