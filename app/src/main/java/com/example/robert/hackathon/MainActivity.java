package com.example.robert.hackathon;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener {
    private static final String TAG = "MainActivity";
    ArrayAdapter<String> adapter;
    private ListView list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button create_schedule= (Button) findViewById(R.id.create_schedule);
        create_schedule.setOnClickListener(this);
        list = (ListView) findViewById(R.id.positions);
        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, new ArrayList<String>());
        list.setAdapter(adapter);

    }

    @Override
    public void onClick(View view) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                this, MainActivity.this, 2017, 6, 10);
        datePickerDialog.show();

    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        Log.i(TAG,"Selected date");
        Intent intent = new Intent(this, MapsActivity.class);
        intent.putExtra("YEAR",datePicker.getYear());
        intent.putExtra("MONTH", datePicker.getMonth());
        intent.putExtra("DAY", datePicker.getDayOfMonth());
        startActivity(intent);
    }
}
