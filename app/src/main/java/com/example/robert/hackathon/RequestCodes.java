package com.example.robert.hackathon;

/**
 * Created by robert on 6/9/17.
 */
public class RequestCodes {
    public final static int GET_NEW_MARKER_DATA = 1;
    public final static int UPDATE_MARKER_DATA = 2;
}
