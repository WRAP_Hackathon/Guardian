package com.example.robert.hackathon.model;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;


public class Position implements IJSONParsable {
	private int id;

	private Date time;

	private double lat;

	private double lon;

	public Position() {
	}
	
	public Position(int id, Date time, double lat, double lon) {
		super();
		this.id = id;
		this.time = time;
		this.lat = lat;
		this.lon = lon;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}	
	
	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	@Override
	public JSONObject toJSON() throws JSONException{
		JSONObject o = new JSONObject();
		
		o.put("id", id);
		o.put("date", new SimpleDateFormat("yyyy/MM/dd HH:mm").format(time));
		o.put("lat", lat);
		o.put("lon", lon);
		
		return o;
	}
}
