package com.example.robert.hackathon.model;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class Schedule implements IJSONParsable {

    private int schedule_id;
    private Date date;

    private List<SimpleTask> tasks;

    public Schedule() {
        tasks = new LinkedList<>();
    }

    public int getSchedule_id() {
        return schedule_id;
    }

    public void setSchedule_id(int schedule_id) {
        this.schedule_id = schedule_id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<SimpleTask> getTasks() {
        return tasks;
    }

    public void setTasks(List<SimpleTask> tasks) {
        this.tasks = tasks;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("schedule_id", schedule_id);
        obj.put("date", new SimpleDateFormat("yyyy/MM/dd").format(date));

        JSONArray arrayOfTasks = new JSONArray();
        for (SimpleTask task : tasks) {
            arrayOfTasks.put(task.toJSON());
        }
        obj.put("tasks", arrayOfTasks);

        return obj;
    }
}
