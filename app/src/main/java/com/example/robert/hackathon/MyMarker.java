package com.example.robert.hackathon;

import com.example.robert.hackathon.model.Location;
import com.example.robert.hackathon.model.SimpleTask;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.Date;

/**
 * Created by robert on 6/9/17.
 */
public class MyMarker {
    MyMarker(Marker marker, int hour, int minute)
    {
        mMarker=marker;
        mHour = hour;
        mMinute=minute;
    }
    private Marker mMarker;
    private int mHour;
    private int mMinute;

    public String getName()
    {
        return mMarker.getTitle();
    }
    public void setName(String name)
    {
        mMarker.setTitle(name);
    }

    public int getHour() {
        return mHour;
    }

    public void setHour(int hour) {
        this.mHour = hour;
    }

    public int getMinute() {
        return mMinute;
    }

    public void setMinute(int minute) {
        this.mMinute = minute;
    }

    public SimpleTask toTask()
    {
        SimpleTask task = new SimpleTask();
        task.setTime(new Date(2017,6,9,mHour,mMinute));
        double lon = mMarker.getPosition().longitude;
        double lat = mMarker.getPosition().latitude;
        task.setLocation(new Location(lat,lon));
        task.setName(mMarker.getTitle());
        return task;
    }

}
