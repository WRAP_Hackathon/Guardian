package com.example.robert.hackathon.managers;

import com.example.robert.hackathon.model.Position;

import java.util.List;

/**
 * Created by APS Drones on 10.06.2017.
 */

public enum PositionsManager {
    INSTANCE;

    List<Position> positionsList;

    public void set(List<Position> p) {
        this.positionsList = p;
    }
}
