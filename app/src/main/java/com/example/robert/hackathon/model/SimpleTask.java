package com.example.robert.hackathon.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SimpleTask implements IJSONParsable{

	protected int task_id;
	protected String name;
	protected Date time;
	protected Location location;


	protected Schedule schedule;

	public SimpleTask() {
		super();
	}

	public SimpleTask(int id, String name) {
		super();
		this.task_id = id;
		this.name = name;
	}

	public int getId() {
		return task_id;
	}

	public void setId(int id) {
		this.task_id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Schedule getSchedule() {
		return schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	public int getMinute(){return time.getMinutes();}

	public int getHours(){return time.getHours();}

	@Override
	public JSONObject toJSON() throws JSONException {
		JSONObject obj = new JSONObject();
		obj.put("task_id", task_id);
		obj.put("name",name);
		obj.put("time", new SimpleDateFormat("HH:mm").format(time));

		obj.put("location", location.toJSON());
		
		return obj;
	}

}
