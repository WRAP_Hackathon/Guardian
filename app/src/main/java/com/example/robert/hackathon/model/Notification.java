package com.example.robert.hackathon.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

public class Notification implements IJSONParsable {
	private int id;

	private Date time;

	private String message;

	private boolean toGuardian;

	private boolean seen;
	

	public Notification() {
	}

	public Notification(int id, Date time, String message, boolean toGuardian, boolean seen) {
		super();
		this.id = id;
		this.time = time;
		this.message = message;
		this.toGuardian = toGuardian;
		this.seen = seen;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isToGuardian() {
		return toGuardian;
	}

	public void setToGuardian(boolean toGuardian) {
		this.toGuardian = toGuardian;
	}

	public boolean isSeen() {
		return seen;
	}

	public void setSeen(boolean seen) {
		this.seen = seen;
	}
	
	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	@Override
	public JSONObject toJSON() throws JSONException{
		JSONObject o = new JSONObject();
		
		o.put("id", id);
		o.put("msg", message);
		o.put("toGuardian", toGuardian);
		o.put("seen", seen);
		o.put("date", new SimpleDateFormat("yyyy/MM/dd HH:mm").format(time));
		
		return o;
	}
}
