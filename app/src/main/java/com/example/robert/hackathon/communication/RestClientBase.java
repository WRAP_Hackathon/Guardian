package com.example.robert.hackathon.communication;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by APS Drones on 09.06.2017.
 */

public abstract class RestClientBase extends AsyncTask<String , Void, JSONObject> {
    private String mMethod;
    private String mURL;
    private String mData;

    public RestClientBase(String pMethod,
                          String pURL) {
        mMethod = pMethod;
        mURL = pURL;
    }

    protected abstract void prepare();

    protected void setData(JSONObject data) {
        mData = data.toString();
    }

    protected void setData(String data) {
        mData = data;
    }

    protected JSONObject doInBackground(String... urls) {
        JSONObject resp = null;
        if (mMethod == null || mMethod.equals("GET")) {
            resp = ResponseParser.extractJSON(get(mURL));
        } else if (mMethod.equals("POST")) {
            resp = ResponseParser.extractJSON(post(mURL, mData));
        } else {
            Log.e(getClass().getName(), "Error ocurred, shouldn't call something without method POST or GET.");
        }
        return resp;
    }

    private HttpResponse post(String pUri, String pData) {
        HttpResponse resp = null;
        try {
            HttpPost frame = new HttpPost(pUri);
            frame.setEntity(new StringEntity(pData));
            frame.setHeader("Accept", "application/json");
            frame.setHeader("Content-type", "application/json");
            resp = new DefaultHttpClient().execute(frame);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            resp = null;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            resp = null;
        } catch (IOException e) {
            e.printStackTrace();
            resp = null;
        }
        return resp;
    }

    private HttpResponse get(String pUri) {
        HttpResponse resp = null;
        try {
            HttpGet frame = new HttpGet(pUri);
            frame.setHeader("Accept", "application/json");
            frame.setHeader("Content-type", "application/json");
            resp = new DefaultHttpClient().execute(frame);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            resp = null;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            resp = null;
        } catch (IOException e) {
            e.printStackTrace();
            resp = null;
        } catch (Exception e) {
            e.printStackTrace();
            resp = null;
        }
        Log.w(getClass().getName(), "\nNULL\n");
        return resp;
    }
}
