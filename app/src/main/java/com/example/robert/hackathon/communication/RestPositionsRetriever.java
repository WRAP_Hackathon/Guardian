package com.example.robert.hackathon.communication;

import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

/**
 * Created by APS Drones on 09.06.2017.
 */

public class RestPositionsRetriever extends RestClientBase {

    private RestPositionsRetriever() {
        super("POST", "http://172.16.102.35:8080/WrapGuardianServer/message/getPositions");
    }

    protected void prepare() {
        try {
//            JSONObject obj = msg.toJSON();
//            setData(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static JSONObject getPositions() {
        JSONObject response = null;
        RestPositionsRetriever task = null;
        try {
            task = new RestPositionsRetriever();
            task.prepare();
            task.execute();
            response = task.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return response;
    }
}