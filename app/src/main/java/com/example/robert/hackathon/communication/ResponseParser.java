package com.example.robert.hackathon.communication;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by APS Drones on 09.06.2017.
 */

public class ResponseParser {
    public static JSONObject extractJSON(HttpResponse pResp) {
        JSONObject pObj = null;
        try {
            if (pResp == null) {
                Log.d(ResponseParser.class.getName(), "Can't parse - no response.");
                return null;
            }

            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(pResp.getEntity().getContent(), "UTF-8"));
            String json = reader.readLine();
            JSONTokener tokener = new JSONTokener(json);
            pObj = new JSONObject(tokener);
        } catch (JSONException e) {
            Log.e(ResponseParser.class.getName(), e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Log.e(ResponseParser.class.getName(), e.getMessage());
            e.printStackTrace();
        }
        return pObj;
    }
}
