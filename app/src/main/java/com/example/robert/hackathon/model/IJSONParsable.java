package com.example.robert.hackathon.model;

import org.json.JSONException;
import org.json.JSONObject;

public interface  IJSONParsable {
	public JSONObject toJSON() throws JSONException;

}
