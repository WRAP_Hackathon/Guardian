package com.example.robert.hackathon;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;

import java.sql.Time;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by robert on 6/9/17.
 */
public class MarkerActivity extends FragmentActivity implements View.OnClickListener {
    public static String TAG = "MarkerActivity";
    private Button ok_button;
    private TimePicker picker;
    private EditText name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marker);

        ok_button = (Button) findViewById(R.id.ok_button);
        ok_button.setOnClickListener(this);
        name = (EditText) findViewById(R.id.name);
        picker = (TimePicker) findViewById(R.id.timePicker);
        Intent intent = getIntent();
        name.setText(intent.getStringExtra("NAME"));


        int hour = new Time(System.currentTimeMillis()).getHours();
        int minute = new Time(System.currentTimeMillis()).getMinutes();

        picker.setCurrentHour(intent.getIntExtra("HOUR",hour));
        picker.setCurrentMinute(intent.getIntExtra("MINUTE", minute));

    }
    //Destroy activity to save memory in mobilephone
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            endActivity();
        }
        return super.onKeyDown(keyCode, event);
    }
    public void endActivity()
    {
        Intent resultIntent = new Intent();
        // TODO Add extras or a data URI to this intent as appropriate.
        resultIntent.putExtra("NAME", name.getText().toString());
        resultIntent.putExtra("MINUTE", picker.getCurrentMinute());
        resultIntent.putExtra("HOUR", picker.getCurrentHour());
        resultIntent.putExtra("MARKER_ID", getIntent().getStringExtra("MARKER_ID"));
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }
    @Override
    public void onClick(View view) {

        endActivity();
    }
}
