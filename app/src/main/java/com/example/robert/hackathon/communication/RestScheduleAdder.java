package com.example.robert.hackathon.communication;

import com.example.robert.hackathon.model.Schedule;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutionException;

/**
 * Created by APS Drones on 09.06.2017.
 */

public class RestScheduleAdder extends RestClientBase {

    private Schedule schedule;

    private RestScheduleAdder(Schedule schedule) {
        super("POST", "http://172.16.102.35:8080/WrapGuardianServer/addSchedule");
        this.schedule = schedule;
    }

    protected void prepare() {
        try {
            JSONObject obj = schedule.toJSON();
            setData(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static JSONObject addSchedule(Schedule schedule) {
        JSONObject response = null;
        RestScheduleAdder task = null;
        try {
            task = new RestScheduleAdder(schedule);
            task.prepare();
            task.execute();
            response = task.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return response;
    }
}