package com.example.robert.hackathon.managers;

import com.example.robert.hackathon.model.Notification;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by APS Drones on 10.06.2017.
 */

public enum MessagesManager {
    INSTANCE;
    List<Notification> messages = new ArrayList<>();

    public List<Notification> getMessages(){
        return messages;
    }

    public void setMessages(List<Notification> nsg){
        this.messages = nsg;
    }

}