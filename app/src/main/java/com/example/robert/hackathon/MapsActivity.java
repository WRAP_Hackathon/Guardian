package com.example.robert.hackathon;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.example.robert.hackathon.communication.RestScheduleAdder;
import com.example.robert.hackathon.model.Schedule;
import com.example.robert.hackathon.model.SimpleTask;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.*;
import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener {
    private HashMap<String, MyMarker> mMarkers = new HashMap<String, MyMarker>();
    private GoogleMap mMap;
    private static final String TAG = "MapsActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        Button save_button = (Button)findViewById(R.id.send_button);
        save_button.setOnClickListener(this);
        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                LatLng coords = place.getLatLng();
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(coords)      // Sets the center of the map to location user
                        .zoom(17)                   // Sets the zoom
                        .build();
                mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                Log.i(TAG, "Place: " + place.getName());
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
            }
        });

    }
    private LatLng mLastClicked;
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //RestScheduleAdder.getSchedule(new Schedule());

        LatLng gdansk = new LatLng(54.372158, 18.638306);
        Marker m = mMap.addMarker(new MarkerOptions().position(gdansk).title("Some task"));
        mMarkers.put(m.getId(), new MyMarker(m, 0,0));

        mMap.moveCamera(CameraUpdateFactory.newLatLng(gdansk));
        final MapsActivity thisActivity = this;

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                //Add marker
                Intent intent = new Intent(thisActivity, MarkerActivity.class);
                startActivityForResult(intent, RequestCodes.GET_NEW_MARKER_DATA);
                mLastClicked = latLng;
                Log.d(TAG, "Clicked=" + latLng);
            }
        });

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                //Update marker
                Intent intent = new Intent(thisActivity, MarkerActivity.class);
                MyMarker mark = mMarkers.get(marker.getId());
                intent.putExtra("NAME", mark.getName());
                intent.putExtra("HOUR", mark.getHour());
                intent.putExtra("MINUTE", mark.getMinute());
                intent.putExtra("MARKER_ID", marker.getId());

                startActivityForResult(intent, RequestCodes.UPDATE_MARKER_DATA);
                Log.d(TAG, "Clicked marker");
                return false;
            }
        });
    }
    private void createMarker(Intent data)
    {
        String name = data.getStringExtra("NAME");
        int hour = data.getIntExtra("HOUR",0);
        int minute = data.getIntExtra("MINUTE",0);
        Marker m = mMap.addMarker(new MarkerOptions().position(mLastClicked).title(name));
        mMarkers.put(m.getId(),new MyMarker(m, hour, minute));
    }
    private void updateMarker(MyMarker marker, Intent data)
    {
        marker.setName(data.getStringExtra("NAME"));
        marker.setHour(data.getIntExtra("HOUR", 0));
        marker.setMinute(data.getIntExtra("MINUTE", 0));
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case (RequestCodes.UPDATE_MARKER_DATA) : {
                if (resultCode == Activity.RESULT_OK) {
                    MyMarker marker = mMarkers.get(data.getStringExtra("MARKER_ID"));
                    if(marker == null)
                        Log.e(TAG,"Marker is null");
                    else
                        updateMarker(marker, data);
                }
                break;

            }
            case (RequestCodes.GET_NEW_MARKER_DATA): {
                if (resultCode == Activity.RESULT_OK) {
                    createMarker(data);
                }
                break;
            }
        }
    }
    private Schedule generateSchedule()
    {
        Schedule schedule = new Schedule();
        ArrayList<SimpleTask> tasks = new ArrayList<>();
        for (MyMarker m : mMarkers.values())
        {
            tasks.add(m.toTask());
        }
        schedule.setTasks(tasks);
        int year = getIntent().getIntExtra("YEAR",0);
        int month = getIntent().getIntExtra("MONTH",0);
        int day = getIntent().getIntExtra("DAY", 0);
        schedule.setDate(new Date(year,month,day));
        return schedule;
    }

    @Override
    public void onClick(View view) {
        Schedule schedule = generateSchedule();
        RestScheduleAdder.addSchedule(schedule);
        finish();
    }
}
