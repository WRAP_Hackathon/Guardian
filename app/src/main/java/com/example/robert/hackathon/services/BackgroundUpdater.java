package com.example.robert.hackathon.services;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.example.robert.hackathon.communication.RestPositionsRetriever;
import com.example.robert.hackathon.managers.PositionsManager;
import com.example.robert.hackathon.model.Notification;
import com.example.robert.hackathon.model.Position;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by APS Drones on 09.06.2017.
 */

public class BackgroundUpdater extends Service {

    Handler timerHandler = new Handler();
    Runnable timerRunnable = new Runnable() {

        @Override
        public void run() {
            try {
                NotificationsManager.INSTANCE.set(getNotifications());
                PositionsManager.INSTANCE.set(getPositions());
            } catch (Exception e) {
                Toast.makeText(BackgroundUpdater.this, "Error parsing tasks: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
            timerHandler.postDelayed(this, 30000);
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        timerHandler.postDelayed(timerRunnable, 0);
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private List<Position> getPositions() throws JSONException, ParseException {
        JSONObject scheduleObj = RestPositionsRetriever.getPositions().getJSONObject("returnable");
        JSONArray positions = scheduleObj.getJSONArray("instance");

        List<Position> positionsList = new LinkedList<>();

        for (int i = 0; i < positions.length(); i++) {
            JSONObject singleTask = positions.getJSONObject(i);
            Position singleTaskInstance = new Position();
            singleTaskInstance.setLat(singleTask.getDouble("lat"));
            singleTaskInstance.setLon(singleTask.getDouble("lon"));
            singleTaskInstance.setTime(new SimpleDateFormat("yyyy/MM/dd HH:mm").parse(singleTask.getString("date")));
            singleTaskInstance.setId(singleTask.getInt("id"));

            positionsList.add(singleTaskInstance);
        }

        return positionsList;
    }

    private List<Notification> getNotifications() throws JSONException, ParseException {
        JSONArray positions = RestPositionsRetriever.getPositions().getJSONArray("instance");

        List<Notification> notifications = new LinkedList<>();

        for (int i = 0; i < positions.length(); i++) {
            JSONObject singleTask = positions.getJSONObject(i);
            Notification singleTaskInstance = new Notification();
            singleTaskInstance.setMessage(singleTask.getString("msg"));
            singleTaskInstance.setSeen(singleTask.getBoolean("seen"));
            singleTaskInstance.setToGuardian(singleTask.getBoolean("toGuardian"));
            singleTaskInstance.setTime(new SimpleDateFormat("yyyy/MM/dd HH:mm").parse(singleTask.getString("date")));

            notifications.add(singleTaskInstance);
        }

        return notifications;
    }
}
