package com.example.robert.hackathon.model;


import org.json.JSONException;
import org.json.JSONObject;

public class Location implements IJSONParsable {

	private int location_id;
	private String name;
	private double latitude;
	private double longitude;

	public Location() {
	}

	public Location(double latitude, double longitude) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public Location(int location_id, String name, double latitude, double longitude) {
		super();
		this.location_id = location_id;
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public int getLocation_id() {
		return location_id;
	}

	public void setLocation_id(int location_id) {
		this.location_id = location_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@Override
	public JSONObject toJSON() throws JSONException {
		JSONObject obj = new JSONObject();

		obj.put("name", name);
		obj.put("latitude", latitude);
		obj.put("longitude", longitude);

		return obj;
	}
}
